# Matomo Yourls

Add Matomo tracking script in redirect page.

### Install
- Simply place the folder "matomo-yourls" in "user/plugins/"
- Insert your Matomo tracking code into "matomo.txt"
- Activate the plugin in admin panel