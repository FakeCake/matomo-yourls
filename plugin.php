<?php
/*
Plugin Name: Matomo Yourls
Plugin URI: https://gitlab.com/FakeCake/matomo-yourls
Description: Matomo Stats for YOURLS
Version: 1.0
Author: FakeCake
*/

// No direct call
if( !defined( 'YOURLS_ABSPATH' ) ) die();

yourls_add_action( 'pre_redirect', 'matomo' );
function matomo( $args ) {
	echo '<!DOCTYPE html><html><head><meta charset="UTF-8"/>';
	echo '<meta http-equiv="refresh" content="1; url=\''.$args[0].'\'"/>';
	echo '</head><body>';
	echo file_get_contents(__DIR__.'/matomo.txt');
	echo '</body></html>';
	die();
}